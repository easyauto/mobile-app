package in.FindAuto.userApp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class RequestSentMask extends Activity {
	
	
	
	@Override
	 public void onCreate(Bundle savedInstanceState) 
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requestsentmask);
        
        TextView editText = (TextView) findViewById(R.id.textView1);
        
        
        Intent intent = getIntent();
        String message = UserApp.sendStatus;//intent.getStringExtra("status").trim();
        UserApp.requestSentMaskActivity=this;
		
		if( message.equalsIgnoreCase("noNet")){
			editText.setText("Trying to connect. Please wait.");        	
		}else if( message.equalsIgnoreCase("sending") ){
        	editText.setText("The request is being sent. Please wait.");      
        	UserApp.sendStatus="none";
        }else if( message.equalsIgnoreCase("http")){
        	editText.setText("Thank you for using findauto service. \nYour request has been received  and is being processed. You will soon receive an SMS with further details.");
        	UserApp.sendStatus="none";
        }else if( message.equalsIgnoreCase("sms")){
        	editText.setText("Thank you for using findauto service. \n We could not find an active network connection. But your request has been sent via SMS. You will soon receive an SMS with further details.");
        	UserApp.sendStatus="none";
        }else if( message.equalsIgnoreCase("failed")){
        	editText.setText("We are sorry. \n Your request could not be sent. We regret the inconvenience caused. Thank you for using findauto service.");
        	UserApp.sendStatus="none";
        }else{
        	Log.e("findAuto","++requestSentMask message=" + message);
        	editText.setText("Trying to connect. Please wait.");        
        }
        
	
		
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
    	UserApp.sendStatus="none";
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}
	
}
