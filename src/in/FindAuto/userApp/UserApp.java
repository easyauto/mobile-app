package in.FindAuto.userApp;



import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;




public class UserApp extends FragmentActivity implements LocationListener {
	static String serverPhoneNumber="9266680802";
	
	static String TAG="findAuto";
	
	EditText  phonetext;
	AutoCompleteTextView messageText,destinationText;
	Button sendButton,updateButton;
	
	LinearLayout progressDisplay;
	TextView progressText;
	ProgressBar progressBar;
	
	String phoneNumber="5554";
	String T = new String();
	String address="";
	Marker	sourceMarker=null,
			destMarker=null;
	
	LatLng  sourceLL=null,
			destLL=null;
	Polyline connectorLine=null;
	Location lastLoc=null;
	
	public static Activity requestSentMaskActivity=null;
	
	public static String  	sourceToSms=null,
							destToSms=null,
							phToSms=null;
	
	public static String sendStatus="none"; //can be none,http,sms,failed, sending
	public static final String PREFS_NAME = "FindAutoPrefs";
	public static final String STORED_PH = "phoneStored";
	
	DialogInterface.OnClickListener smsDialogClickListener;
	
	List<Address> addresses = null;
	
	Boolean mapClickedTextChange=false; //will be true immediately before a text field is changed by map Click 
	//ArrayAdapter 	sourceAdapter=null,
	//				destAdapter=null;
	
	 //denotes which location is being picked from map. can be messageText or DestinationText
	EditText currentChoiceText=messageText;
	
	AsyncTask<String, Void, LatLng> reverseGeoCoder=null;
	
	private LocationManager locationManager;
	
	private static final long MIN_TIME = 400;
	private static final float MIN_DISTANCE = 1000;
	
//	 Google Map
    private GoogleMap googleMap;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smsdemo);
        
        messageText = (AutoCompleteTextView)findViewById(R.id.messageText); //source
        destinationText=(AutoCompleteTextView)findViewById(R.id.Destination); //destination

        phonetext=(EditText)findViewById(R.id.phoneid);
        Button sendButton = (Button) findViewById(R.id.sendButton);
        Button updateButton = (Button) findViewById(R.id.updateButton);
        
        progressDisplay = (LinearLayout) findViewById(R.id.progressDisplay);
        progressText = (TextView) findViewById(R.id.progressText);
        progressBar =  (ProgressBar) findViewById(R.id.progressBar1);
        /*************** onCreate MAP *********************************************/
        try {
            // Loading map
            initilizeMap();
 
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        googleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
            	
        	   
        	     // Ensure that a Geocoder services is available
    	        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD  ){
    	                					//	&& Geocoder.isPresent()) {
    	            // Show the activity indicator
    	           // mActivityIndicator.setVisibility(View.VISIBLE);
    	            /*
    	             * Reverse geocoding is long-running and synchronous.
    	             * Run it on a background thread.
    	             * Pass the current location to the background task.
    	             * When the task finishes,
    	             * onPostExecute() displays the address.
    	             */
    	            new UpdateMapTask().execute(point);
    	        }
        	        
    	        if( currentChoiceText==messageText){
                 	drawMarkerSource(point);
                 	sourceLL=point;
                 }else if( currentChoiceText==destinationText){
                 	drawMarkerDest(point);
                 	destLL=point;
                 }
    	        
    	        mapClickedTextChange=true;
    	        currentChoiceText.setText(point.toString());
                 
                 if( sourceLL!=null && destLL!=null){
                 	if( connectorLine!=null){
                 		connectorLine.remove();
                 	}
                 	connectorLine=googleMap.addPolyline(new PolylineOptions()
                 													.geodesic(true)
     										                        .add(sourceLL)  
     										                        .add(destLL)
     										                        );  
                 }
        	    
            }
        });
  
        
        /******************** onCreate Reverse Geocoding & Autofill ******************/
        
        messageText.setAdapter(new AutoCompleteAdapter(this));
        destinationText.setAdapter(new AutoCompleteAdapter(this));
        /********************onCreate  GPS ************************************************/
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        
        LocationManager manager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            //Ask the user to enable GPS
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Location Manager");
            builder.setMessage("Would you like to enable GPS?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //Launch settings, allowing user to make a change
                    Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(i);
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //No location service, no Activity
                   // finish();
                }
            });
            builder.create().show();
        }
        
        if(manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
	        try{
	        	locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this); //You can also use LocationManager.GPS_PROVIDER and LocationManager.PASSIVE_PROVIDER
	        }catch(Exception e){
	        	 Toast.makeText(getApplicationContext(), "Location request failed", Toast.LENGTH_LONG).show();
	        	 e.printStackTrace();
	        }
	        
	        if( locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)!=null){
	        	lastLoc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
	        }
        }
        
        if(manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
	        try{
	        	locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, this); //You can also use LocationManager.GPS_PROVIDER and LocationManager.PASSIVE_PROVIDER
	        }catch(Exception e){
	        	 Toast.makeText(getApplicationContext(), "Location request failed", Toast.LENGTH_LONG).show();
	        	 e.printStackTrace();
	        }
	        
	        if( locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)!=null){
	        	lastLoc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	        }
        }
        
     //   if( lastLoc!=null){
      //  	updateLocationDetails(lastLoc);
      //  }
        
        /*************** onCreate storage *************************/
        
        SharedPreferences storage= getSharedPreferences(PREFS_NAME, 0);
        if( storage.contains(STORED_PH) ){
        	String storedPh = storage.getString(STORED_PH, "");
            phonetext.setText(storedPh);
        }

        /*************** onCreate  Listeners **********************/
        
        messageText.setOnFocusChangeListener(new OnFocusChangeListener() {          

            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                	currentChoiceText=messageText;
            }
        });
        
        destinationText.setOnFocusChangeListener(new OnFocusChangeListener() {          

            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                	currentChoiceText=destinationText;
            }
        });
        
        messageText.addTextChangedListener(new TextWatcher(){
	        public void onTextChanged(CharSequence s, int start, int before,int count) {
	        	if( mapClickedTextChange==false ){
		        	sourceLL=null; //if text is entered manually, remove the old location taken from map
		        	if( sourceMarker!=null){
		        		sourceMarker.remove();
		        	}
		        	if( connectorLine!=null){
		        		connectorLine.remove();
		        	}
	        	}else{
	        		mapClickedTextChange=false; //dont remove the new marker if the content is from map
	        	}
	        }

			@Override
			public void afterTextChanged(Editable s) {
				if( reverseGeoCoder!=null){
					reverseGeoCoder.cancel(true);
				}
				reverseGeoCoder=new RevGeoCode().execute(s.toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
			}
     
        });
        
        destinationText.addTextChangedListener(new TextWatcher(){
	        public void onTextChanged(CharSequence s, int start, int before,int count) {
	        	if( mapClickedTextChange==false ){
		        	destLL=null; //if text is entered manually, remove the old location taken from map
		        	if( destMarker!=null){
		        		destMarker.remove();
		        	}
		        	if( connectorLine!=null){
		        		connectorLine.remove();
		        	}
	        	}else{
	        		mapClickedTextChange=false;//dont remove the new marker if the content is from map
	        	}
	        }

			@Override
			public void afterTextChanged(Editable s) {
				if( reverseGeoCoder!=null){
					reverseGeoCoder.cancel(true);
				}
				reverseGeoCoder=new RevGeoCode().execute(s.toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
			}
     
        });
        
        phonetext.addTextChangedListener(new TextWatcher(){
	        public void onTextChanged(CharSequence s, int start, int before,int count) {	        
	        	SharedPreferences storage= getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
				SharedPreferences.Editor editor = storage.edit();
				editor.putString(STORED_PH, s.toString());
				editor.commit();	
	        }

			@Override
			public void afterTextChanged(Editable s) {
				
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
			}
     
        });
        
        sendButton.setOnClickListener(new View.OnClickListener() {
		  public void onClick(View view) {			  			  
			  String sText="",dText="";
			  if( sourceLL!=null)  {
				  sText=sourceLL.toString();
			  }else if( messageText.getText().toString().length()!=0){
				  sText= messageText.getText().toString();
			  }
			  
			  if( destLL!=null)  {
				  dText=destLL.toString();
			  }else if( destinationText.getText().toString().length()!=0){
				  dText= destinationText.getText().toString();
			  }
			  
			  if( dText.length()!=0 && dText.length()!=0 && phonetext.getText().toString().length()!=0){
				  showProgressText("Sending request...");
				  
				  new Connection().execute(sText,dText, phonetext.getText().toString());
				  
				  	if( requestSentMaskActivity!=null){
						requestSentMaskActivity.finish();
					}				
					Intent intent = new Intent(UserApp.this, RequestSentMask.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					sendStatus="sending";
				    //intent.putExtra("status", "sending");
					getApplicationContext().startActivity(intent);
					
			  }else{
				  Toast.makeText(getApplicationContext(),
	                        "Please enter all values", Toast.LENGTH_SHORT)
	                        .show();
			  }
		  }
        });
        
        updateButton.setOnClickListener(new View.OnClickListener() {
  		  public void onClick(View view) {	
  			  
  	        if( lastLoc!=null){
  	        	zoomToLoc(lastLoc);
  	        	
  	        	LatLng latLng = new LatLng(lastLoc.getLatitude(), lastLoc.getLongitude());
  	        	messageText.setText(latLng.toString());
  	        	
  	        	new AsyncUseCurrentLoc().execute(lastLoc); //geocode the lastLoc

  	        	sourceLL=new LatLng(lastLoc.getLatitude(),lastLoc.getLongitude());
            	drawMarkerSource(sourceLL);
            	
                if( sourceLL!=null && destLL!=null){
                	if( connectorLine!=null){
                		connectorLine.remove();
                	}
                	connectorLine=googleMap.addPolyline(new PolylineOptions()
                													.geodesic(true)
    										                        .add(sourceLL)  
    										                        .add(destLL)
    										                        );  
                }

  	        }
  	       }
          });
        
        
        smsDialogClickListener = new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            switch (which){
	            case DialogInterface.BUTTON_POSITIVE:
	                //Yes button clicked
	            	sendSMS(sourceToSms,destToSms,phToSms);		
		    		showProgressText("SMS has been sent since network is unavailable");
	                break;
	
	            case DialogInterface.BUTTON_NEGATIVE:
	                //No button clicked
	            	UserApp.sendStatus="failed";
		    		showProgressText("Request not sent");
	                break;
	            }
			  	
	            if( requestSentMaskActivity!=null){
					requestSentMaskActivity.finish();
				}	
            	Intent intent = new Intent(UserApp.this, RequestSentMask.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				getApplicationContext().startActivity(intent);
				
	        }
	    };
        

    }
    

/********* onCreate over *****************/
    
/*************** Map functions *****************/
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.map)).getMap();
 
            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
    
    //customize the markers
    private void drawMarkerSource(LatLng ll){
    	if( sourceMarker!=null){
    		sourceMarker.remove();
    	}
    	sourceMarker=googleMap.addMarker(new MarkerOptions()
    											.title("Pickup")
    											.position(ll)
    											//.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
    											);
    	 if( sourceLL!=null && destLL!=null){
          	if( connectorLine!=null){
          		connectorLine.remove();
          	}
          	connectorLine=googleMap.addPolyline(new PolylineOptions()
          													.geodesic(true)
										                        .add(sourceLL)  
										                        .add(destLL)
										                        );  
          }
    	 if( sourceLL!=null && destLL!=null){
    		 LatLngBounds LLb= new LatLngBounds(sourceLL, sourceLL);
    		 LLb=LLb.including(destLL);
    		 CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(LLb, 100);
    		 googleMap.animateCamera(cameraUpdate);
    	 }else{
    		 zoomToLoc(ll);
    	 }
    }
    
    private void drawMarkerDest(LatLng ll){
    	if( destMarker!=null){
    		destMarker.remove();
    	}
    	destMarker=googleMap.addMarker(new MarkerOptions()
    											.title("Destination")
    											.position(ll)
    											.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
    											);
    	 if( sourceLL!=null && destLL!=null){
          	if( connectorLine!=null){
          		connectorLine.remove();
          	}
          	connectorLine=googleMap.addPolyline(new PolylineOptions()
          													.geodesic(true)
										                        .add(sourceLL)  
										                        .add(destLL)
										                        );  
          }
    	 
    	 if( sourceLL!=null && destLL!=null){
    		 LatLngBounds LLb= new LatLngBounds(sourceLL, sourceLL);
    		 LLb=LLb.including(destLL);
    		 CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(LLb, 100);
    		 googleMap.animateCamera(cameraUpdate);
    	 }else{
    		 zoomToLoc(ll);
    	 }

    }
    
    public void zoomToLoc(Location loc){
    	LatLng latLng = new LatLng(loc.getLatitude(), loc.getLongitude());
	    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
	    googleMap.animateCamera(cameraUpdate);
    }
    
    public void zoomToLoc(LatLng ll){
    	LatLng latLng = new LatLng(ll.latitude, ll.longitude);
	    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
	    googleMap.animateCamera(cameraUpdate);
    }
    /*
    public void updateLocationDetails(Location loc){
    	
	    LatLng latLng = new LatLng(loc.getLatitude(), loc.getLongitude());
	    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
	    googleMap.animateCamera(cameraUpdate);
	   // locationManager.removeUpdates(this);
	    
	    
	    
    	Geocoder geocoder;
	    List<Address> addresses = null;
	    geocoder = new Geocoder(this, Locale.getDefault());
	    try {
			addresses = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
	    if( addresses!=null){
	    	address = addresses.get(0).getAddressLine(0);
	    }
    }
    */
    
    
    private class AsyncUseCurrentLoc extends AsyncTask<Location, Void, Void >{
    	 @Override
			protected Void doInBackground(Location... params) {
				Location loc=params[0];
				Geocoder geocoder;    	    
	    	    geocoder = new Geocoder( getBaseContext(), Locale.getDefault());
	    	    try {
					addresses = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(),1); //updates addresses
	    		} catch (IOException e) {
	    			e.printStackTrace();
	    		}
				return null;
			}
    	 
    	 @Override
		 protected void onPreExecute() {
		     // Set activity indicator visibility to "gone"
			// progressText.setText("Updating location");
		     //progressDisplay.setVisibility(View.VISIBLE);
			 showProgressTextWithBar("Getting Current Location");
		 }
		 
		 @Override
		 protected void onPostExecute(Void param ) {
		     // Set activity indicator visibility to "gone"
		    // progressDisplay.setVisibility(View.GONE);
			 hideProgress();
		     // Display the results of the lookup.
			 
			 
			 if( addresses == null ){
     	    	Toast.makeText(getApplicationContext(), "No internet access! Exact address cannot be resolved but location has been marked", Toast.LENGTH_SHORT).show(); //remove!
     	    }else{
     	    	address = addresses.get(0).getAddressLine(0);           
     	    	mapClickedTextChange=true;
     	    	messageText.setText(address);
     	    }    
		 }
    }
    
    
	@Override
	public void onLocationChanged(Location loc) {
		lastLoc=loc;
		 Toast.makeText(getApplicationContext(),
                 "Current Location Updated", Toast.LENGTH_SHORT)
                 .show();
		zoomToLoc(loc);	    
	}
	
	 private class UpdateMapTask extends AsyncTask<LatLng, Void, List<Address> > {
	 
		 /**
		  * A method that's called once doInBackground() completes. Turn
		  * off the indeterminate activity indicator and set
		  * the text of the UI element that shows the address. If the
		  * lookup failed, display the error message.
		  */
		 @Override
			protected List<Address> doInBackground(LatLng... params) {
				LatLng point=params[0];
				Geocoder geocoder;    	    
	    	    geocoder = new Geocoder( getBaseContext(), Locale.getDefault());
	    	    try {
					addresses = geocoder.getFromLocation(point.latitude, point.longitude,1);
	    		} catch (IOException e) {
	    			e.printStackTrace();
	    		}
				return addresses;
			}
		 
		 @Override
		 protected void onPreExecute() {
		     // Set activity indicator visibility to "gone"
			// progressText.setText("Updating location");
		     //progressDisplay.setVisibility(View.VISIBLE);
			 showProgressTextWithBar("Updating Location");
		 }
		 
		 @Override
		 protected void onPostExecute(List<Address> params) {
		     // Set activity indicator visibility to "gone"
		    // progressDisplay.setVisibility(View.GONE);
			 hideProgress();
		     // Display the results of the lookup.
			 
			 if( addresses == null ){
     	    	Toast.makeText(getApplicationContext(), "No internet access! Exact address cannot be resolved but location has been marked", Toast.LENGTH_LONG).show(); //remove!
     	    }else{
     	    	address = addresses.get(0).getAddressLine(0);       
     	    	mapClickedTextChange=true;
     	    	currentChoiceText.setText(address);
     	    }    
		 }
	}
	 /********************** Reverse GeoCode **************************/
	 
	 private class RevGeoCode extends AsyncTask<String, Void, LatLng > {
		 
		 /**
		  * A method that's called once doInBackground() completes. Turn
		  * off the indeterminate activity indicator and set
		  * the text of the UI element that shows the address. If the
		  * lookup failed, display the error message.
		  */
		 @Override
			protected LatLng doInBackground(String... params) {
			 LatLng p =null;
			 try {
				 String locName=params[0];
				 Geocoder geocoder = new Geocoder( getBaseContext(), Locale.getDefault());
				 List<Address> addresses = geocoder.getFromLocationName(locName + " , Kerala", 5);
				 String strCompleteAddress = "";
				 if (addresses.size() > 0) {

					 for( int i=0; i<addresses.size(); i++){
						 
						 Log.e(TAG,"++addresses++" + addresses.toString());
						 String CountryCode=addresses.get(i).getCountryCode();
						 boolean inIndia=false,
								 inKerala=false;
						 
						 if( CountryCode!=null ){
							 inIndia=CountryCode.toString().trim().equalsIgnoreCase("IN");
						 }
						 
						 Address address=addresses.get(i);
						 if( address!=null ){
							 inIndia=address.toString().trim().equalsIgnoreCase("kerala");
						 }
						 
						 if( inIndia || inKerala ){
							 p = new LatLng(addresses.get(0).getLatitude(),addresses.get(0).getLongitude() );
							 break;
						 }
					 }
						 
				 }
			 } catch (IOException e) {
				 
				 e.printStackTrace();
				 
			 }
			 return p;
			 
			}
		 
		 @Override
		 protected void onPreExecute() {
		     // Set activity indicator visibility to "gone"
			// progressText.setText("Updating location");
		     //progressDisplay.setVisibility(View.VISIBLE);
			 showProgressTextWithBar("Updating Map");
		 }
		 
		 @Override
		 protected void onPostExecute(LatLng p) {
		     // Set activity indicator visibility to "gone"
		    // progressDisplay.setVisibility(View.GONE);
			 hideProgress();
		     // Display the results of the lookup.
			 
			 mapClickedTextChange=true;
     	    if( p!=null){   
     	    	mapClickedTextChange=true;
     	    	if( currentChoiceText==messageText){
     	    		sourceLL=p;
     	    		drawMarkerSource(p);
     	    	}else if( currentChoiceText==destinationText ){
     	    		destLL=p;
     	    		drawMarkerDest(p);
     	    	}
     	    }    
		 }
	}

	 /**************** Progress Updates *****************/
	 
	 void hideProgress(){		 	     
	     progressBar.setVisibility(View.VISIBLE);
	     progressDisplay.setVisibility(View.GONE); //default status
	 }
	 
	 void showProgressText(String s){
		 progressText.setText(s);
	     progressDisplay.setVisibility(View.VISIBLE);
	     progressBar.setVisibility(View.GONE);
	 }
	 
	 void showProgressTextWithBar(String s){
		 progressText.setText(s);
	     progressDisplay.setVisibility(View.VISIBLE);
	     progressBar.setVisibility(View.VISIBLE);
	 }
	 
    /****************** HTTP Functions ******************/ 

	    
    public class Connection extends AsyncTask<String , Void , Void> {

		public boolean requestSuccess=false;

		@Override
		protected Void doInBackground(String... params) {
    		
    		runOnUiThread(new Runnable() {
	    	  public void run() {
	    		  showProgressText("Sending request...");
	    	      }
	    	  });    		
    		
			connect(params[0],params[1],params[2]); //source lat & long, dest lat & long, phone
			return null;
		}	
		
		@Override
		protected void onPostExecute(Void result) {
			
			Log.w(TAG,"++onPostExecute of Connect HTTP++");
		    
			if( sendStatus=="noNet"){
			//	sendStatus="smsConfirmation";
				if( requestSentMaskActivity!=null){
					requestSentMaskActivity.finish();
				}				
			 AlertDialog.Builder builder = new AlertDialog.Builder(UserApp.this);
			    builder.setMessage("We could not connect to server? Send request via SMS?")
			    		.setPositiveButton("Yes", smsDialogClickListener)
			        	.setNegativeButton("No", smsDialogClickListener);
			    
			    AlertDialog alert = builder.create();
	            alert.show();
				if( requestSentMaskActivity!=null){
					requestSentMaskActivity.finish();
				}	
			//}else if (sendStatus=="smsConfirmation"){ //do nothing
			}else{
		            
				Intent intent = new Intent(UserApp.this, RequestSentMask.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				//intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			    intent.putExtra("status", sendStatus);
				getApplicationContext().startActivity(intent);
			}
		}
 
    }
    
  

    
    public void connect(final String source, final String dest,final String ph){
    	try {
			  List<NameValuePair> n = new ArrayList<NameValuePair>();
			  n.add(new BasicNameValuePair("source", source ));
			  n.add(new BasicNameValuePair("dest", dest ));
			  n.add(new BasicNameValuePair("ph", ph ));
//			  n.add(new BasicNameValuePair("debug", "true" ));
//			  n.add(new BasicNameValuePair("user", "AUTO" ));
//			  n.add(new BasicNameValuePair("platform", "ANDROID" ));
			  
			  sourceToSms=source;
			  destToSms=dest;
			  phToSms=ph;
					 
			  Log.w(TAG,"++calling HTTP++");
			  
			  HttpClient client = new DefaultHttpClient();
			  HttpPost post = new HttpPost("http://findauto.in/process_app_message.php");		
			  post.setEntity(new UrlEncodedFormEntity(n, "UTF-8"));  
			  
		      HttpResponse response = client.execute(post);
		     
		      int responseCode = response.getStatusLine().getStatusCode();
		      
		      HttpEntity entity = response.getEntity();
		      String contentText = EntityUtils.toString(entity);
		      
		      //Toast.makeText(getApplicationContext(), Integer.toString(responseCode) , Toast.LENGTH_LONG).show();
		      
		      //if( headerText.trim().equalsIgnoreCase("GotRequest")){ //ignore white spaces		    
		      if(responseCode==200 && contentText.trim().equalsIgnoreCase("ok")){
			    	  sendStatus="http";		    	  
				      runOnUiThread(new Runnable() {
				    	  public void run() {
				    		  //Toast.makeText(getApplicationContext(), "Your request has been received and is being processed", Toast.LENGTH_LONG).show();
				    		  showProgressText("Your request has been received and is under processing");
				    		  			    		 			    		 
				    	      }
				    	  });
		      }else{
		    	 // sendStatus="sms";		    	  
		      }
		} catch (Exception e) {
			// TODO: handle exception
			//sendStatus="failed";
			sendStatus="noNet";

			//e.printStackTrace();
		}
    	
    	
    }
    
    /********************* SMS functions *****************************/ 
    public void sendSMS(String source, String dest,String ph) {

    	Log.w(TAG,"++sending SMS++");
    	
        SmsManager smsManager = SmsManager.getDefault();
        String msg="FAuto "+source+"*"+dest;
        smsManager.sendTextMessage(serverPhoneNumber, null, msg, null, null);
        
        ContentValues values = new ContentValues(); 
        values.put("address", serverPhoneNumber.toString() ); 
        values.put("body", msg );         
        getContentResolver().insert(Uri.parse("content://sms/sent"), values);
        
        sendStatus="sms";		 
		
        /*
        runOnUiThread(new Runnable() {
	    	  public void run() {
	    		  //Toast.makeText(getApplicationContext(), "Message sent", Toast.LENGTH_LONG).show();
		    	  showProgressText("SMS request Sent");
	    	      }
	    	  });
	    	  */
    }
   /* 
    public void invokeSMSApp(String source, String dest,String ph) {
        Intent smsIntent = new Intent(Intent.ACTION_VIEW);

        smsIntent.putExtra("sms_body", "FAuto "+source+"*"+dest); //"Hello World!"); 
        smsIntent.putExtra("address", serverPhoneNumber); //"0123456789");
        smsIntent.setType("vnd.android-dir/mms-sms");

        startActivity(smsIntent);
        
        runOnUiThread(new Runnable() {
	    	  public void run() {
	    		  Toast.makeText(getApplicationContext(), "Message sent", Toast.LENGTH_LONG).show();
	    	      }
	    	  });
    }
    
    public void saveInSent() {
    	ContentValues values = new ContentValues(); 
        
        values.put("address", phoneNumber); 
                  
        values.put("body", messageText.getText().toString()); 
                  
        getContentResolver().insert(Uri.parse("content://sms/sent"), values);
    }
*/
    /********************** Autofill *************************************/
   /**/ 
    private class AutoCompleteAdapter extends ArrayAdapter<Address> implements Filterable {
 
		private LayoutInflater mInflater;
		private Geocoder mGeocoder;
		private StringBuilder mSb = new StringBuilder();
	 
		public AutoCompleteAdapter(final Context context) {
			super(context, -1);
			mInflater = LayoutInflater.from(context);
			mGeocoder = new Geocoder(context);
		}
	 
		@Override
		public View getView(final int position, final View convertView, final ViewGroup parent) {
			final TextView tv;
			if (convertView != null) {
				tv = (TextView) convertView;
			} else {
				tv = (TextView) mInflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
			}
	 
			tv.setText(createFormattedAddressFromAddress(getItem(position)));
			return tv;
		}
	 
		private String createFormattedAddressFromAddress(final Address address) {
			mSb.setLength(0);
			final int addressLineSize = address.getMaxAddressLineIndex();
			for (int i = 0; i < addressLineSize; i++) {
				mSb.append(address.getAddressLine(i));
				if (i != addressLineSize - 1) {
					mSb.append(", ");
				}
			}
			return mSb.toString();
		}
	 
		@Override
		public Filter getFilter() {
			Filter myFilter = new Filter() {
				@Override
				protected FilterResults performFiltering(final CharSequence constraint) {
					List<Address> addressList = null;
					if (constraint != null) {
						try {
							addressList = mGeocoder.getFromLocationName((String) constraint + " , Kerala", 5);
						} catch (IOException e) {
						}
					}
					if (addressList == null) {
						addressList = new ArrayList<Address>();
					}
	 
					final FilterResults filterResults = new FilterResults();
					filterResults.values = addressList;
					filterResults.count = addressList.size();
	 
					return filterResults;
				}
	 
				@SuppressWarnings("unchecked")
				@Override
				protected void publishResults(final CharSequence contraint, final FilterResults results) {
					clear();
					for (Address address : (List<Address>) results.values) {
						add(address);
					}
					if (results.count > 0) {
						notifyDataSetChanged();
					} else {
						notifyDataSetInvalidated();
					}
				}
	 
				@Override
				public CharSequence convertResultToString(final Object resultValue) {
					return resultValue == null ? "" : ((Address) resultValue).getAddressLine(0);
				}
			};
			return myFilter;
		}
	}
    /**/
    /**************** Standard app management functions *****************/
    
	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}        
	
    @Override
    protected void onResume() {
        super.onResume();
        initilizeMap();
    }
    
    @Override
    protected void onStop() {


        super.onStop();
    }

    @Override
    protected void onDestroy() {


        super.onDestroy();
    }

}

